TASKAPP
==========

TASKAPP backend on Django with Docker

Requirements
------------

* `Docker <https://docs.docker.com/install>`_
  — Install Docker for different operating system. See documentation.
* `Docker Compose <https://docs.docker.com/compose/install/>`_
  — Install Docker Compose for MacOS, Windows, and Linux
* `Gitlab Runner <https://docs.gitlab.com/runner/>`_
  — Install gitlab runner for macOS, Windows, Linux and FreeBSD
* `Python 3.8 <https://www.python.org/>`_
  — Programming language
* `Django <https://www.djangoproject.com/>`_
  — The django requirements packages are in the requirements.txt file
* `Postgres <https://www.postgresql.org/>`_
  — The user credentials are in the environment variable file called: .env
* `Nginx <https://nginx.org/en/>`_
  — Nginx is used as a reverse proxy.
* `RabbitMQ <https://www.rabbitmq.com/>`_
  — RabbitMQ is a message broker by queues.
* `Django Rest Framework <https://www.django-rest-framework.org/>`_
  — Django Rest Framework is a tool for building Web APIs.

LOCAL
================

* **Build or rebuild web project**: ``$ docker-compose build``
* **Start web project as a service in docker**: ``$ docker-compose up``
* **Stops and removes containers, networks, volumes, and images in docker**: ``$ docker-compose down``
* **Create Django project**: ``$ docker-compose run web django-admin.py startproject <project_name> .``
* **Launch the development server**: ``$ docker-compose run web python manage.py runserver 0.0.0.0:8000``
* **Run makemigrations command on Django project**: ``$ docker-compose run web python manage.py makemigrations``
* **Run migrate command on Django project**: ``$ docker-compose run web python manage.py migrate``
* **Create superuser on Django project**: ``$ docker-compose run web python manage.py createsuperuser``
* **Run test command on Django project**: ``$ docker-compose run web python manage.py test``

TASKAPP DEV
==============

Build or rebuild web project with taskapp-dev.yml:

.. code-block:: bash

    $ docker-compose -f taskapp-dev.yml build

Start web project as a service in docker.:

.. code-block:: bash

    $ docker-compose -f taskapp-dev.yml up

Stops and removes containers, networks, volumes, and images in docker.:

.. code-block:: bash

    $ docker-compose -f taskapp-dev.yml down