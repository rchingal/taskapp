FROM python:3.8-slim-buster

ARG DJANGO_ENV

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED=1

ENV DJANGO_DIR=/taskapp

RUN mkdir $DJANGO_DIR

WORKDIR $DJANGO_DIR

RUN pip install --upgrade pip

COPY requirements/base.txt $DJANGO_DIR/
COPY requirements/$DJANGO_ENV.txt $DJANGO_DIR/

RUN pip install -r $DJANGO_ENV.txt

COPY . $DJANGO_DIR/