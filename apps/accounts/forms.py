import unicodedata
from django import forms

from django.utils.translation import gettext as _
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm, PasswordResetForm, SetPasswordForm

from .models import User

class UserForm(forms.ModelForm):
    """
    A form that allows entering the user's data.
    """
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'gender', 'address', 'phone', 'avatar', 'document_number', 'is_active', ]
        labels = {
            'groups': _('Perfil'),
        }
        widgets = {
            'email': forms.EmailInput(attrs={'placeholder': 'username@userdirection.com', 'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'placeholder': 'First name', 'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'placeholder': 'Last name', 'class': 'form-control'}),
            'gender': forms.Select(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'placeholder': 'Address', 'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'placeholder': 'Phone', 'class': 'form-control'}),
            'document_number': forms.NumberInput(
                attrs={'placeholder': '1234678', 'class': 'form-control font-family-quicksand font-size-16 px-2'}),
            'avatar': forms.FileInput(attrs={'class': 'form-control custom-file-input'}),
            'is_active': forms.CheckboxInput( attrs={
                'class': 'form-control bootstrap-switch',
            }),
        }


class SignUpForm(UserForm, UserCreationForm):
    name = 'signup'
    password1 = forms.CharField(
        label=_("Password"),
        strip=True,
        widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control'}),
        help_text=password_validation.password_validators_help_text_html(),
    )

    password2 = forms.CharField(
        label=_("Password confirmation"),
        required=True,
        widget=forms.PasswordInput(attrs={'placeholder': 'Password confirmation', 'class': 'form-control'}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        password_validation.validate_password(password2, self.user)
        return password2


class EditProfileForm(UserForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'gender', 'address', 'phone', 'avatar', 'type_document', 'document_number', ]

        widgets = {
            'first_name': forms.TextInput(attrs={'placeholder': 'Jhon A.', 'class': 'form-control font-family-quicksand font-size-16 px-2'}),
            'last_name': forms.TextInput(attrs={'placeholder': 'Doe', 'class': 'form-control font-family-quicksand font-size-16 px-2'}),
            'gender': forms.Select(attrs={'class': 'form-control font-family-quicksand font-size-16'}),
            'type_document': forms.Select(attrs={'class': 'form-control font-family-quicksand font-size-16'}),
            'document_number': forms.NumberInput(attrs={'placeholder': '1234678', 'class': 'form-control font-family-quicksand font-size-16 px-2'}),
            'address': forms.TextInput(attrs={'placeholder': 'Cra. 3 #123-123', 'class': 'form-control font-size-16'}),
            'phone': forms.TextInput(attrs={'placeholder': '+57 3333333', 'class': 'form-control font-family-quicksand font-size-16 px-2'}),
            'avatar': forms.FileInput(attrs={'class': 'form-control font-family-quicksand font-size-16'}),
        }
        exclude = ['email', 'username', 'password', ]


class UsernameField(forms.CharField):
    def to_python(self, value):
        return unicodedata.normalize('NFKC', super(UsernameField,self).to_python(value))


class CustomAuthenticationForm(AuthenticationForm):
    """Custom autentication form extends from the Django AuthenticationForm to add the classes for render"""
    error_css_class = 'is-invalid'

    username = UsernameField(label=_("Username"), widget=forms.TextInput(
        attrs={'placeholder': 'Username', 'autofocus': True, 'class':'form-control'})
    )
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class':'form-control'}),
    )


class CustomPasswordResetForm(PasswordResetForm):
    """
    Custom Password reset form extends from the Django PasswordResetForm to add the classes for render
    """
    error_css_class = 'is-invalid'
    email = forms.EmailField(label=_("Email"), max_length=254, widget=forms.EmailInput(
        attrs={'autofocus': True, 'placeholder': 'username@domain.com', 'class': 'form-control'})
    )


class CustomSetPasswordForm(SetPasswordForm):
    """
    A form that lets a user change set their password without entering the old password
    """
    error_css_class = 'is-invalid'
    new_password1 = forms.CharField(
        label=_("New password "),
        widget=forms.PasswordInput(attrs={'placeholder': 'New password *', 'class':'form-control'}),
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        label=_("Confirm new password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'placeholder': 'Repeat the new password*','class':'form-control'}),
    )