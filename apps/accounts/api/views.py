from django.contrib import auth
from django.utils.translation import gettext_lazy as _
from rest_framework import status, generics
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken

from apps.tasks.api.views import TaskList
from .serializers import UserSerializer
from .permissions import IsOwnerOrReadOnly
from .pagination import UserPageNumberPagination

from ..models import User
from ..forms import SignUpForm
from ..tasks import send_mail

class UserList(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.filter(is_superuser=False)
    name = 'Users List'
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]
    authentication_class = (TokenAuthentication,)
    pagination_class = UserPageNumberPagination


class Login(APIView):
    name = 'Api Login'
    permission_classes = (AllowAny, )

    def post(self, request, version):
        """
        Send the auth data to celery and if it returns true we create jwt
        The jwt is created with refresh and access,
        which will be used to access a protected route with the bearer flag
        tThe session that sends the expiration time to the cookie is created,
        the time is configured in SETTINGS
        """
        auth_result = ''
        username = request.data.get('username')
        password = request.data.get('password')

        user = User.objects.filter(email=username).first()
        if user and user.check_password(password):
            refresh = RefreshToken.for_user(user)
            res = {
                "refresh": str(refresh),
                "access": str(refresh.access_token),
            }
            return Response(res, status=status.HTTP_200_OK)
        else:
            res = {
                "message": 'Invalid credentials',
            }
            return Response(res, status=status.HTTP_401_UNAUTHORIZED)


class Logout(APIView):
    name = 'Api Logout'
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]
    authentication_class = (TokenAuthentication,)

    def post(self, request, version):
        """
        From the cache we get the jwt and send it to the blacklist
        if there is the token is exp also the session and cookies are destroyed
        """
        refresh = RefreshToken.for_user(request.user)
        auth.logout(request)
        if refresh:
            refresh.blacklist()
        res = {
            "detail": 'Session ending',
        }
        return Response(res, status=status.HTTP_205_RESET_CONTENT)


class SignupApiView(APIView):
    """
    Create a new user and register this account
    """
    name = 'Sign Up'
    permission_classes = (AllowAny, )

    def post(self, request, version):
        """
        register a new user
        :param request:
        :param version:
        :return: success or serialized errors
        """
        group = self.request.data.get('group', 'host')
        form = SignUpForm(self.request.POST, self.request.FILES)
        if not form.is_valid():
            errors = dict(form.errors.items())
            return Response([{
                'status': 'error',
                'msg': _('can not create user, data errors'),
                'data': errors
            }],
                status=status.HTTP_400_BAD_REQUEST)
        user = form.save()
        serializer = UserSerializer(user)

        refresh = RefreshToken.for_user(user)
        auth_tokens = {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }
        send_mail.delay(request.get_host(), user.id)
        return Response([{
            'status': 'success',
            'msg': _('user {} created successfully.').format(user.get_full_name()),
            'version': version,
            'data': serializer.data,
            'auth_tokens': auth_tokens
        }], status=status.HTTP_200_OK)


class ApiRoot(generics.GenericAPIView):
    name = 'api-root'
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, *args, **kwargs):
        return Response({
            'users': reverse(UserList.name, request=request, kwargs=kwargs),
            'tasks': reverse(TaskList.name, request=request, kwargs=kwargs),
        })