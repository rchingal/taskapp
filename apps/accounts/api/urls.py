from django.urls import path, include

from .views import ApiRoot, UserList, Logout, Login, SignupApiView

urlpatterns = [
    # Accounts
    path('accounts/users/', UserList.as_view(), name=UserList.name),
    path('accounts/login/', Login.as_view(), name=Login.name),
    path('accounts/signup/', SignupApiView.as_view(), name=SignupApiView.name),
    path('accounts/logout/', Logout.as_view(), name=Logout.name),

    path('', include('apps.tasks.api.urls') ),

    path('', ApiRoot.as_view(), name=ApiRoot.name),
]
