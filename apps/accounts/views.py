from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.db import transaction
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import views, login
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, UpdateView

from apps.commons.helpers import uw_add_alert

from .models import User
from .tasks import send_mail
from .decorators import is_superuser
from .forms import CustomSetPasswordForm, CustomAuthenticationForm, SignUpForm, EditProfileForm


class SignUpView(CreateView):
    """
    Register a New user to the app creating a :model:`accounts.User`.
    **Context**
    ``object``
        An instance of :model:`accounts.User`.
    **Template:**
    :template:`accounts/user_form.html`
    """
    name = 'signup'
    model = User
    form_class = SignUpForm
    template_name = 'accounts/registration/signup.html'
    success_url = reverse_lazy('accounts:login')
    success_msg = _('You have successfully registered on the Elenas platform.!')

    @transaction.atomic
    def form_valid(self, form):
        """If the form is valid, save the user as inactive and a confirmation email is sent."""
        sid = transaction.savepoint()
        try:
            user = form.save(commit=False)
            user.username = self.request.POST.get('username')
            user.set_password( self.request.POST.get('password'))
            user.is_active = True
            user.save()
            uw_add_alert(self.request, self.success_msg, "success")
            transaction.savepoint_commit(sid)
            transaction.on_commit(lambda: send_mail.delay(self.request.get_host(), user.id))
            return super(SignUpView, self).form_valid(form)
        except Exception as e:
            uw_add_alert(self.request, e, "error")
            transaction.savepoint_rollback(sid)
        return super().form_invalid(form)


@method_decorator(is_superuser, name='dispatch')
class LoginView(views.LoginView):
    name = 'login'
    form_class = CustomAuthenticationForm
    template_name = 'accounts/registration/login.html'


    def get_success_url(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            return reverse_lazy('admin:index')
        else:
            return reverse_lazy('accounts:my_profile')

    def form_valid(self, form):
        """Security check complete. Log the user in."""
        login(self.request, form.get_user())
        return HttpResponseRedirect(self.get_success_url())


@method_decorator(login_required, name='dispatch')
class LogoutView(views.LogoutView):
    name = 'logout'
    template_name = 'accounts/registration/logged_out.html'


class PasswordResetConfirmView(views.PasswordResetConfirmView):
    form_class = CustomSetPasswordForm
    template_name = 'accounts/registration/password_reset_confirm.html'
    success_msg = _('Your password has been changed successfully!')

    def form_valid(self, form):
        """If the form is valid, save the user as inactive and a confirmation email is sent."""
        form.save()
        uw_add_alert(self.request, self.success_msg, "success")
        return redirect('accounts:login')

@method_decorator(login_required, name='dispatch')
class MyProfile(UpdateView):
    """
    View to see the profile of the user authenticated.
    """
    name = 'my_profile'
    model = User
    form_class = EditProfileForm
    success_url = reverse_lazy('accounts:my_profile')
    success_msg = _('Your profile has been updated successfully!')

    def get_object(self, **kwargs):
        ## this is to get the object without the user id, override the id with the current user
        return get_object_or_404(self.model.objects.select_related(None),
                                 pk=self.request.user.id)

    def form_valid(self, form):
        uw_add_alert(self.request, self.success_msg, "success")
        return super(MyProfile, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class EditProfileView(UpdateView):
    """
    View to edit the profile of the user authenticated.
    """
    name = 'edit_profile'
    model = User
    form_class = EditProfileForm
    template_name = 'accounts/user_detail.html'
    success_url = reverse_lazy('accounts:my_profile')
    success_msg = _('Your profile has been updated successfully.')

    def form_valid(self, form):
        uw_add_alert(self.request, self.success_msg, "success")
        return super(EditProfileView, self).form_valid(form)