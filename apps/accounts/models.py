from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractUser, BaseUserManager
from simple_history.models import HistoricalRecords


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        import random
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


def create_route_avatar(instance, filename):
    path = 'avatars/{username}/{name}'.format(
        username=instance.username, name=filename
    )
    return path


class PersonAbstract(models.Model):
    """
    Person Abstract model based in profile information
    """

    class GENDER(models.TextChoices):
        NO_SPECIFY = 'no_specify', _('No specify')
        MEN = 'men', _('Men')
        WOMEN = 'women', _('Women')

    class TYPE_DOCUMENT(models.TextChoices):
        NO_SPECIFY = 'no_specify', _('No specify')
        CITIZENDSHIP_CARD = 'citizenship_card', _('Citizenship card')
        IDENTITY_CARD = 'identity_card', _('Identity card')
        FOREIGNER_ID = 'foreigner_id', _('Foreigner id')
        PASSPORT = 'passport', _('Passport')

    gender = models.CharField(max_length=10, choices=GENDER.choices,
        default=GENDER.NO_SPECIFY, blank=True, null=True, verbose_name=_('Gender'))
    address = models.CharField(max_length=30, blank=True, null=True,
        verbose_name=_('Address'), help_text=_('Address.'))
    phone = models.CharField(max_length=30, blank=True, null=True,
        verbose_name=_('Phone'), help_text=_('user phone.'))
    document_number = models.BigIntegerField(null=True, blank=True,
        unique=True, verbose_name=_('Document number'),
        help_text=_('This number is unique and cannot be changed once registered..'))
    type_document = models.CharField(max_length=20, choices=TYPE_DOCUMENT.choices,
        default=TYPE_DOCUMENT.NO_SPECIFY, blank=True, null=True, verbose_name=_('Type document'))

    class Meta:
        abstract = True


class User(PersonAbstract, AbstractUser):
    """
    User model based in abstract user for profile information
    """
    # making the email username
    username = None
    email = models.EmailField(unique=True, error_messages={'unique': _("A user with this email already exists."), },
        verbose_name=_('Email address'),
        help_text=_('The email address is the same username.'))
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    avatar = models.ImageField(max_length=255, upload_to=create_route_avatar, blank=True, null=True,
        verbose_name=_('Profile picture'), help_text=_('User profile picture'))

    objects = UserManager()

    history = HistoricalRecords()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        if not self.first_name or not self.last_name:
            # without full name return the email username.
            return '{}'.format(self.email.split('@')[0])
        else:
            return '{}'.format(self.get_full_name())

    @property
    def email_username(self):
        return '{}'.format(self.email.split('@')[0])

    @property
    def get_avatar(self):
        if self.avatar:
            return self.avatar.url
        else:
            return None