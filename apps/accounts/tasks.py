import os
from celery import task, shared_task
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.utils.translation import get_language, gettext as _

from .models import User

@shared_task
def send_mail(domain, id):
    user = User.objects.filter(id=id).first()
    if user:
        subject = _('Activate your account in elenas')
        plain_text_message = get_template('accounts/mail/notification_register.txt')
        html_message = get_template('accounts/mail/notification_register.html')
        data = {
            'user': user,
            'domain': domain,
        }
        subject, from_email, to = subject, os.environ.get('CONTACT_FORM'), user.email
        text_content = plain_text_message.render(data)
        html_content = html_message.render(data)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    pass