# Generated by Django 3.0.2 on 2020-09-27 16:22

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(help_text='descriptive task name', max_length=100, verbose_name='description')),
                ('is_completed', models.BooleanField(default=False, help_text='task is completed?', verbose_name='is completed?')),
                ('created_on', models.DateTimeField(auto_now_add=True, help_text='created date', null=True, verbose_name='created date')),
                ('changed_on', models.DateTimeField(auto_now=True, help_text='updated date', null=True, verbose_name='updated date')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Task',
                'verbose_name_plural': 'Tasks',
            },
        ),
    ]
