from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Task

class TaskForm(forms.ModelForm):
    """
    Task form for tasks.
    """
    class Meta:
        model = Task
        fields = '__all__'
        """
        widgets = {
            'name': forms.TextInput(
                attrs={'placeholder': 'Task', 'autofocus': True, 'class': 'form-control search-round'}
            ),
        }
        """
        exclude = ['user', ]