from rest_framework import serializers

from ..models import Task

class TaskSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    #user = serializers.SlugRelatedField(queryset=Task.objects.all(), slug_field='id')

    class Meta:
        model = Task
        fields = [
            'id',
            'description',
            'is_completed',
            'user',
        ]

    def get_user(self, obj):
        return obj.user.email_username