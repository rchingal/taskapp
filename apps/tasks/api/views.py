from django.utils.translation import gettext_lazy as _
from rest_framework import status, generics
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication


from .serializers import TaskSerializer
from .permissions import IsOwnerOrReadOnly
from .pagination import TaskPageNumberPagination

from ..models import Task


class TaskList(generics.ListCreateAPIView):
    """
    Tasl List or Task Create View
    """
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    name = 'Tasks List'
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]
    authentication_class = (TokenAuthentication,)
    pagination_class = TaskPageNumberPagination

    def get_queryset(self):
        user = self.request.user  # Current User
        is_completed = self.request.GET.get('status')
        description = self.request.GET.get('description')
        kwargs = {}
        if is_completed:
            kwargs['is_completed'] = is_completed
        if description:
            kwargs['description__icontains'] = description

        return Task.objects.filter(user=user, **kwargs)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class TaskUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    name = 'Task Update and destroy'
    lookup_field = 'pk'
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]