from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination

class TaskLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 5
    max_limit = 10000

class TaskPageNumberPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 10000