from django.urls import path, include

from .views import TaskList, TaskUpdateDestroyView

urlpatterns = [
    # Tasks
    path('tasks/', TaskList.as_view(), name=TaskList.name),
    path('tasks/<int:pk>/', TaskUpdateDestroyView.as_view(), name=TaskUpdateDestroyView.name),
]
