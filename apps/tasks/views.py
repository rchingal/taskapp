from django.db.models import Q
from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView

from apps.commons.views import CreateAndUpdateViewMixin
from apps.commons.helpers import uw_add_alert

from .models import Task
from .forms import TaskForm

class TasksListView(ListView):
    """
    List view for tasks
    """
    name = 'task_list'
    model = Task
    paginate_by = 5

    def get_queryset(self, tab=None):
        """
        Queryset giving only the current forms
        """
        keyword = self.request.GET.get('keyword', None)
        queryset = self.model.objects.filter(user=self.request.user).order_by('-id')
        if keyword:
            queryset = self.model.objects.filter(
                Q(description__icontains=keyword) | Q(user__first_name__icontains=keyword) | Q(user__last_name__icontains=keyword)
            )
        return queryset



class TodoListView(ListView):
    """
    List view for fields
    """
    name = 'todo_list'
    model = Task
    template_name = 'tasks/todo_list.html'


class TaskView(CreateAndUpdateViewMixin):
    """
    Tasks view (create and update)
    """
    form_class = TaskForm
    model = Task
    success_url = reverse_lazy("tasks:task_list")
    success_msg = _('Your data has been saved correctly!')

    def form_valid(self, form, **kwargs):
        """
        Validate and save the tasks with create or update option
        """
        try:
            obj = form.save(commit=False)
            obj.user_id = self.request.user.id
            obj.save()
            uw_add_alert(self.request, self.success_msg, "success")
            return redirect(self.success_url)
        except Exception as e:
            uw_add_alert(self.request, e, "error")
        return super().form_invalid(form)

    def form_invalid(self, form, **kwargs):
        print(form)
        return super().form_invalid(form)


@method_decorator(login_required, name='dispatch')
class TaskDeleteView(DeleteView):
    """
    Task delete view for Shipment
    """
    name = 'task_delete'
    model = Task
    success_url = reverse_lazy('tasks:task_list')