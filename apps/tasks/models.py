from django.db import models
from django.utils.translation import gettext_lazy as _
from simple_history.models import HistoricalRecords


from apps.accounts.models import User
from apps.commons.models import AuditBaseAbstract

class Task(AuditBaseAbstract):
    """
    Task model.
    """
    name = models.CharField(max_length=100, verbose_name=_('name'),
        help_text=_('Descriptive task name'))
    description = models.TextField(verbose_name=_('description'),
        help_text=_('Task description'), blank=True, null=True)
    is_completed = models.BooleanField(default=False, verbose_name=_('is completed?'),
        help_text=_('Check if the task is complete'))
    user = models.ForeignKey(User, on_delete=models.CASCADE, )

    history = HistoricalRecords()

    class Meta:
        verbose_name = _("Task")
        verbose_name_plural = _("Tasks")

    def __str__(self):
        return "{} - {}".format(self.description, self.user)