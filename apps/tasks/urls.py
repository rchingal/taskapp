from django.urls import path

from .views import *

app_name = "tasks"

urlpatterns = [
    path('tasks/', TasksListView.as_view(), name=TasksListView.name),
    path('tasks/create', TaskView.as_view(), name='task_create'),
    path('tasks/<int:pk>/update/', TaskView.as_view(), name='task_update'),
    path('tasks/<int:pk>/delete/', TaskDeleteView.as_view(), name=TaskDeleteView.name),
]