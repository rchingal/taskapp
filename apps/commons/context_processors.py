from django.urls import reverse
from django.utils.translation import gettext as _


def Menu(request):
    options = {}
    if request.user.is_authenticated:
        options.update({
            'menu': [
                {'name': _('todo list'), 'url': reverse('tasks:task_list'), 'icon': 'fas fa-list', 'submenu': None},
                #{'name': _('todo list'), 'url': reverse('tasks:todo_list'), 'icon': 'far fa-list-alt', 'submenu': None},
            ],
        })
        options.update({
            'menu_user': [
                {'name': _('my profile'), 'url': reverse('accounts:my_profile'), 'icon': 'feather icon-user', },
                {'name': _('logout'), 'url': reverse('accounts:logout'), 'icon': 'feather icon-power', 'last': True},
            ],
        })

        for obj in options['menu']:
            if obj['submenu']:
                for opc in obj['submenu']:
                    if request.path == opc['url']:
                        opc['active'] = True
                        obj['active'] = True
            if request.path == obj['url']:
                obj['active'] = True
    else:
        options.update({
            'menu_anonymous': [
                {'name': _('login'), 'url': reverse('accounts:login'), 'icon': 'fas fa-sign-in-alt', },
            ],
        })
    return options