
from django.db import models
from django.utils.translation import gettext_lazy as _


class AuditBaseAbstract(models.Model):
    created_on = models.DateTimeField(
    	auto_now_add=True,
    	verbose_name=_('created date'),
        help_text=_('created date'),
        blank=True,
        null=True,
    )
    changed_on = models.DateTimeField(
    	auto_now=True,
    	verbose_name=_('updated date'),
        help_text=_('updated date'),
        blank=True,
        null=True,
    )
    created_by = models.ForeignKey(
    	'accounts.User',
    	verbose_name="Created by",
    	help_text=_("Who created it?"),
    	on_delete=models.CASCADE,
    	related_name="%(class)s_created_by",
    	blank=True,
        null=True,
	)
    changed_by = models.ForeignKey(
    	'accounts.User',
    	verbose_name="Changed by",
    	help_text=_("Who changed it?"),
        on_delete=models.CASCADE,
        related_name="%(class)s_changed_by",
        blank=True,
        null=True,
    )

    class Meta:
        abstract = True